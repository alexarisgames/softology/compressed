Alex Aris Games - Note ::

Here are a collection of games that I have worked on and designed over the last 30 years. The platforms range from the Commodore 64, Commodore Amiga and PC. Some of the games may quote a website I used to have www.alexarisgames.co.uk. This is no longer active, but I still can be contacted through the following social media outlets :

twitter : @HeadingtonBard

The games within this collection are *FREE*!
Enjoy, play and let everyone know!

- Alex Aris ( 2021 )

